from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files
from OVS.essential_scripts.login_user import login_user
from time import sleep


def go_to_admin(driver):
    email = get_information_from_text_files(
        base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials\\',
        directory='right_credentials',
        location='email.txt'
    )

    password = get_information_from_text_files(
        base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials\\',
        directory='right_credentials',
        location='password.txt'
    )

    login_user(
        driver=driver,
        username=email,
        password=password
    )

    sleep(2)
    administration = driver.find_element_by_xpath('/html/body/div[9]/a')
    administration.click()
