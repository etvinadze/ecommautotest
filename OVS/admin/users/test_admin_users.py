import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from OVS.essential_scripts.login_user import login_user
from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def go_to_admin_users(self):
        driver = self.driver
        email = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials\\',
            directory='right_credentials',
            location='email.txt'
        )

        password = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials\\',
            directory='right_credentials',
            location='password.txt'
        )

        login_user(
            driver=driver,
            username=email,
            password=password
        )

        sleep(2)
        administration = driver.find_element_by_xpath('/html/body/div[9]/a')
        administration.click()

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, '/html/body/div[3]/div[2]/div/ul/li[4]/a'))
        )
        users_first_level = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[4]/a')
        users_first_level.click()

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, '/html/body/div[3]/div[2]/div/ul/li[4]/ul/li[1]/a'))
        )
        users_second_level = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[4]/ul/li[1]/a')
        users_second_level.click()

    def test_filters(self):
        driver = self.driver
        self.go_to_admin_users()

        # Email Filter
        with self.subTest():
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'SearchEmail')))
            email_filter = driver.find_element_by_id('SearchEmail')
            email_filter.send_keys('erekletesttest@gmail.com')
            driver.find_element_by_id('search-customers').click()

            sleep(1)
            result_email = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[2]').text

            self.assertEqual('erekletesttest@gmail.com', result_email)

        # First and Last name Filter
        with self.subTest():
            email_filter.clear()
            driver.find_element_by_id('search-customers').click()

            first_name = driver.find_element_by_id('SearchFirstName')
            last_name = driver.find_element_by_id('SearchLastName')
            first_name.send_keys('gurbangul')
            last_name.send_keys('berdimuhamedov')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_name = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[3]').text
            self.assertEqual('gurbangul berdimuhamedov', result_name)

        # Company Filter
        with self.subTest():
            first_name.clear()
            last_name.clear()
            driver.find_element_by_id('search-customers').click()

            company = driver.find_element_by_id('SearchCompany')
            company.send_keys('wandio')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_company = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[5]').text
            self.assertEqual('wandio', result_company)

        # Phone Filter
        with self.subTest():
            company.clear()
            driver.find_element_by_id('search-customers').click()

            phone = driver.find_element_by_id('SearchPhone')
            phone.send_keys('551500291')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_phone = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[6]').text
            self.assertEqual('551500291', result_phone)
            # phone.clear()
            # driver.find_element_by_id('search-customers').click()

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
