import unittest
from OVS.admin.catalog.go_to_catalog import go_to_catalog
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()

    @staticmethod
    def go_to_product_attributes(driver):
        go_to_catalog(driver=driver)

        driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[2]/ul/li[6]/a').click()
        driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[2]/ul/li[6]/ul/li[1]/a').click()

    def test_catalog_product_attributes(self):
        driver = self.driver
        self.go_to_product_attributes(driver=driver)

        # Add Attribute
        with self.subTest():
            driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/div[1]/div/a').click()

            # Set Attribute Name
            driver.find_element_by_id('Name').send_keys('testattribute')

            # Save Attribute
            driver.find_element_by_xpath('//*[@id="productattribute-form"]/div[1]/div/button[1]').click()

            self.assertEqual(
                driver.find_element_by_xpath('//*[@id="products-grid"]/tbody/tr[3]/td[1]').text,
                'testattribute'
            )

        # Remove Attribute
        with self.subTest():
            # Go to my added Attribute
            driver.find_element_by_xpath('//*[@id="products-grid"]/tbody/tr[3]/td[2]/a').click()

            # Delete Attribute
            driver.find_element_by_id('productattribute-delete').click()

            # Confirm Deletion
            driver.find_element_by_xpath(
                '//*[@id="productattributemodel-Delete-delete-confirmation"]/div/div/form/div/div[2]/button'
            ).click()

            with self.assertRaises(NoSuchElementException):
                driver.find_element_by_xpath('//*[@id="products-grid"]/tbody/tr[3]/td[2]/a')

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
