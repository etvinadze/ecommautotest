import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def test_searchbar(self):
        driver = self.driver

        search_bar_id = 'small-searchterms'
        search_bar = driver.find_element_by_id(search_bar_id)

        search_bar.send_keys("ტესტ პროდუუქტი")

        with self.subTest():
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
                By.XPATH, '/html/body/div[9]/div/div[1]/div[4]/div[1]/ul/li/a')))
            small_menu_item = driver.find_element_by_xpath('/html/body/div[9]/div/div[1]/div[4]/div[1]/ul/li/a')
            small_menu_item.click()

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
                By.XPATH, '//*[@id="product-details-form"]/div/div/div[2]/div[1]/div[1]/h1')))

            result_string = driver.find_element_by_xpath(
                '//*[@id="product-details-form"]/div/div/div[2]/div[1]/div[1]/h1'
            ).text
            self.assertEqual(result_string, "ᲢᲔᲡᲢ ᲞᲠᲝᲓᲣᲣᲥᲢᲘ")

        driver.back()

        # სწორად სერჩავს თუ არა მაგის ტესტი
        with self.subTest():
            search_key_xpath = '//*[@id="small-search-box-form"]/input[2]'
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, search_key_xpath)))

            search_bar = driver.find_element_by_id(search_bar_id)
            search_bar.send_keys("ტესტ პროდუუქტი")

            search_key = driver.find_element_by_xpath(search_key_xpath)
            search_key.click()

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
                By.XPATH, "/html/body/div[9]/div/div[7]/div/div[2]/div[3]/div/div/div/div/div[2]/h2/a")))

            result_string = driver.find_element_by_xpath(
                '/html/body/div[9]/div/div[7]/div/div[2]/div[3]/div/div/div/div/div[2]/h2/a'
            ).text
            self.assertEqual(result_string, "Ტესტ Პროდუუქტი")

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
