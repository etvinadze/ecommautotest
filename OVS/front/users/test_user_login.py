import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from OVS.essential_scripts.login_user import login_user
from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_login_on_wrong_credentials(self):
        driver = self.driver

        login_user(
            driver=driver,
            username="jafjkhfjkads@wandio.com",
            password="fdaskj312321"
        )

        error_message = driver.find_element_by_xpath(
            '/html/body/div[9]/div/div[7]/div/div/div[2]/div/div/div[2]/form/div[1]'
        )

        self.assertTrue(error_message.is_displayed())

    def test_login_on_right_credentials(self):
        driver = self.driver
        email_location = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='email.txt'
        )

        password_location = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='password.txt'
        )

        login_user(
            driver=driver,
            username=email_location,
            password=password_location
        )

        profile_xpath = '/html/body/div[10]/div[1]/div[1]/div[1]/ul/li[1]/a'
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, profile_xpath)))
        profile = driver.find_element_by_xpath(profile_xpath)

        self.assertTrue(profile.is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
