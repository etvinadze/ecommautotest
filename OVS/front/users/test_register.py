import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def register_user(self, first_name, last_name, phone_number, directory):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.directory = directory

        driver = self.driver
        driver.get("https://ovs.wandio.dev")

        login_button_xpath = '/html/body/div[9]/div[1]/div[1]/div[1]/ul/li[2]/a'
        login_button = driver.find_element_by_xpath(login_button_xpath)

        login_button.click()

        register_xpath = "/html/body/div[9]/div[1]/div[7]/div/div/div[3]/div[1]/div[3]/div[2]/input"
        register_button = driver.find_element_by_xpath(register_xpath)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, register_xpath)))
        sleep(1)
        register_button.click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'FirstName')))
        first_name_location = driver.find_element_by_id('FirstName')
        first_name_location.send_keys(first_name)

        last_name_location = driver.find_element_by_id('LastName')
        last_name_location.send_keys(last_name)

        birth_day = driver.find_element_by_xpath(
            '/html/body/div[9]/div[1]/div[7]/div/form/div/div[2]/div[1]/div[1]/div[2]/div[4]/div/select[1]/option[5]'
        )
        birth_day.click()

        birth_month = driver.find_element_by_xpath(
            '/html/body/div[9]/div[1]/div[7]/div/form/div/div[2]/div[1]/div[1]/div[2]/div[4]/div/select[2]/option[3]'
        )
        birth_month.click()

        birth_year = driver.find_element_by_xpath(
            '/html/body/div[9]/div[1]/div[7]/div/form/div/div[2]/div[1]/div[1]/div[2]/div[4]/div/select[3]/option[81]'
        )
        birth_year.click()

        email_location = driver.find_element_by_id('Email')
        email_location.send_keys(get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory=directory,
            location='email.txt'
        ))

        phone_number_location = driver.find_element_by_id('Phone')
        phone_number_location.send_keys(phone_number)

        password = driver.find_element_by_id('Password')
        password.send_keys(get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory=directory,
            location='password.txt'
        ))

        confirm_password = driver.find_element_by_id('ConfirmPassword')
        confirm_password.send_keys(get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory=directory,
            location='password.txt'
        ))

        register_user_button = driver.find_element_by_id('register-button')
        register_user_button.click()

    def test_register_existing_user(self):
        driver = self.driver
        self.register_user(
            first_name='gurbangul',
            last_name='berdimuhamedov',
            phone_number='595555555',
            directory='register_credentials_on_existing_user'
        )

        sleep(1)

        registration_error_text = driver.find_element_by_xpath(
            '/html/body/div[9]/div[1]/div[7]/div/form/div/div[2]/div[1]/div[1]/ul/li'
        ).text
        self.assertEqual(
            registration_error_text, "აღნიშნული ელ. ფოსტით მომხმარებელი უკვე დარეგისტრირებულია"
        )

    # not working for now
    def test_register_new_user(self):
        driver = self.driver

        self.register_user(
            first_name='Erekle',
            last_name='Tvinadze',
            phone_number='595555555',
            directory='register_credentials_on_new_user'
        )

        sleep(1)

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
