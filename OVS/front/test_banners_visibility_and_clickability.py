import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def test_banners(self):
        driver = self.driver

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
            By.ID, 'WidgetSlider-home_page_main_slider-4'))
        )
        self.assertTrue(driver.find_element_by_id('WidgetSlider-home_page_main_slider-4').is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
