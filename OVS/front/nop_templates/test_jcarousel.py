import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def go_to_product(self):
        driver = self.driver

        man_category = driver.find_element_by_xpath('/html/body/div[9]/div/div[2]/ul[1]/li[2]/a')
        hover = ActionChains(driver).move_to_element(man_category)
        hover.perform()

        jeans_category_xpath = '/html/body/div[9]/div/div[2]/ul[1]/li[2]/div/div/div[1]/div[1]/div/div[1]/a'
        jeans_category = driver.find_element_by_xpath(jeans_category_xpath)
        jeans_category.click()

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
        )))

        first_product = driver.find_element_by_xpath(
            '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
        )
        first_product.click()

    def test_jcarousel(self):
        driver = self.driver
        self.go_to_product()

        sleep(3)

        # jcarousel არ იტვირთება თუ ქვემოთ არ დასქროლა მომხმარებელმა, ამიტომ საჭიროა რო დავსქროლო მეც
        # (1080 არის მონიტორის რეზოლუციიდან სიმაღლე)
        driver.execute_script("window.scrollTo(0, 1080)")
        sleep(1)
        driver.execute_script("window.scrollTo(0, 1080)")

        # Test Image visibility in jcarousel
        with self.subTest():
            product_picture_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[1]/a/img',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[1]/a/img',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[1]/a/img',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[1]/a/img'
            ]

            for product_picture in product_picture_xpaths:
                self.assertTrue(driver.find_element_by_xpath(product_picture).is_displayed())

        # Test Product Titles in jcarousel
        with self.subTest():
            product_title_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[2]/h2/a',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[2]/h2/a',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[2]/h2/a',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[2]/h2/a',
            ]
            for product_title in product_title_xpaths:
                self.assertTrue(driver.find_element_by_xpath(product_title).is_displayed())

        # Test Score in jcarousel
        with self.subTest():
            product_score_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[2]/div[1]/div',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[2]/div[1]/div',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[2]/div[1]/div',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[2]/div[1]/div'
            ]

            for product_score in product_score_xpaths:
                self.assertTrue(driver.find_element_by_xpath(product_score).is_displayed())

        # Test Price in jcarousel
        with self.subTest():
            product_price_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[2]/div[3]/div[1]/span',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[2]/div[3]/div[1]/span',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[2]/div[3]/div[1]/span',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[2]/div[3]/div[1]/span'
            ]

            for product_price in product_price_xpaths:
                self.assertTrue(driver.find_element_by_xpath(product_price).is_displayed())

        # Test add to cart button visibility in jcarousel
        with self.subTest():
            addtocart_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[2]/div[3]/div[2]/div/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[2]/div[3]/div[2]/div/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[2]/div[3]/div[2]/div/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[2]/div[3]/div[2]/div/input'
            ]

            for addtocart in addtocart_xpaths:
                self.assertTrue(driver.find_element_by_xpath(addtocart).is_displayed())

        # Test add to wishlist button visibility in jcarousel
        with self.subTest():
            addto_wishlist_xpaths = [
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[5]/div/div/div[2]/div[3]/div[2]/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[6]/div/div/div[2]/div[3]/div[2]/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[7]/div/div/div[2]/div[3]/div[2]/input',
                '/html/body/div[9]/div/div[7]/div/div[2]/div/div/div[2]/div/div/div[8]/div/div/div[2]/div[3]/div[2]/input'
            ]

            for addto_wishlist in addto_wishlist_xpaths:
                self.assertTrue(driver.find_element_by_xpath(addto_wishlist).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()
