import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from OVS.front.catalog.go_to_product import go_to_product


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def test_product_details_visibility(self):
        driver = self.driver
        go_to_product(driver=driver)

        product_details_list_xpaths = [
            '/html/body/div[9]/div/div[7]/div/div[1]/ul',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[1]/div/div[1]/div/div[1]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[1]/div/div[2]/div/div',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[1]/h1',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[2]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[3]/div[1]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[3]/div[2]',
            '//*[@id="product-details-form"]/div/div/div[2]/div[1]/div[4]/div[1]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[5]/div',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[1]/div[6]/div',
            '//*[@id="quickTab-description"]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[3]/div[3]/div[1]/dl/dd[1]/ul',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[3]/div[3]/div[1]/dl/dd[2]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[3]/div[3]/div[3]/div/div[1]',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[3]/div[3]/div[3]/div/div[2]/input',
            '/html/body/div[9]/div/div[7]/div/div[2]/form/div/div/div[2]/div[3]/div[3]/div[4]/input'
        ]

        WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.XPATH, product_details_list_xpaths[0])))
        for product_detail in product_details_list_xpaths:
            with self.subTest(product_detail=product_detail):
                self.assertTrue(driver.find_element_by_xpath(product_detail).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
