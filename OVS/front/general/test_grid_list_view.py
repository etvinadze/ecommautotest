import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def select_man_jeans_category(self):
        driver = self.driver
        driver.get("https://ovs.wandio.dev")

        man_category = driver.find_element_by_xpath('/html/body/div[9]/div/div[2]/ul[1]/li[2]/a')
        hover = ActionChains(driver).move_to_element(man_category)
        hover.perform()

        jeans_category_xpath = '/html/body/div[9]/div/div[2]/ul[1]/li[2]/div/div/div[1]/div[1]/div/div[1]/a'
        jeans_category = driver.find_element_by_xpath(jeans_category_xpath)
        jeans_category.click()

    def test_grid_list_view(self):
        driver = self.driver

        self.select_man_jeans_category()

        # Grid View
        with self.subTest():
            driver.find_element_by_id('js-grid-mode').click()

            sleep(2)

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[2]'))
            )

            self.assertEqual(True, driver.find_element_by_xpath(
                '//*[@id="filter-product-container"]/div/div/div[2]/div[4]'
            ).is_displayed())

        # List View
        with self.subTest():
            driver.find_element_by_id('js-list-mode').click()

            sleep(1)

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[2]'))
            )

            self.assertEqual(True, driver.find_element_by_xpath(
                '//*[@id="filter-product-container"]/div/div/div[2]/div[4]'
            ).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
