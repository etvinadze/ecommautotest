import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def test_sorts(self):
        driver = self.driver

        man_category = driver.find_element_by_xpath('/html/body/div[9]/div/div[2]/ul[1]/li[2]/a')
        hover = ActionChains(driver).move_to_element(man_category)
        hover.perform()

        jeans_category_xpath = '/html/body/div[9]/div/div[2]/ul[1]/li[2]/div/div/div[1]/div[1]/div/div[1]/a'
        jeans_category = driver.find_element_by_xpath(jeans_category_xpath)
        jeans_category.click()

        sleep(2)

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.ID, 'select2-js-products-orderby-container'))
        )
        driver.find_element_by_xpath(
            '//*[@id="filter-product-container"]/div/div/div[2]/div[3]/div[1]/div[1]/span/span[1]/span'
        ).click()

        # A-Z Sort
        with self.subTest():
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '/html/body/span/span/span[2]/ul/li[2]'
            )))

            sort_a_z = driver.find_element_by_xpath('/html/body/span/span/span[2]/ul/li[2]')
            sort_a_z.click()

            sleep(1.5)

            first_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
            first_result_text = driver.find_element_by_xpath(first_result_xpath).text
            first_result_text.split()
            first_result_word = first_result_text[0]

            last_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[16]/div/div[2]/h2/a'
            last_result_text = driver.find_element_by_xpath(last_result_xpath).text
            last_result_text.split()
            last_result_word = last_result_text[0]

            self.assertEqual(first_result_word, "5")
            self.assertEqual(last_result_word, "S")

        sleep(2)
        driver.find_element_by_xpath('//*[@id="select2-js-products-orderby-container"]').click()

        # Z-A Sort
        with self.subTest():
            sort_z_a_xpath = '/html/body/span/span/span[2]/ul/li[3]'
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_z_a_xpath)))
            driver.find_element_by_xpath(sort_z_a_xpath).click()
            sleep(1.5)

            first_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
            first_result_text = driver.find_element_by_xpath(first_result_xpath).text
            first_result_text.split()
            first_result_word = first_result_text[0]

            last_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[16]/div/div[2]/h2/a'
            last_result_text = driver.find_element_by_xpath(last_result_xpath).text
            last_result_text.split()
            last_result_word = last_result_text[0]

            self.assertEqual(first_result_word, "Ი")
            self.assertEqual(last_result_word, "S")

        sleep(2)
        driver.find_element_by_xpath('//*[@id="select2-js-products-orderby-container"]').click()

        # Price from low to high Test
        with self.subTest():
            sort_low_to_high_xpath = '/html/body/span/span/span[2]/ul/li[4]'
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_low_to_high_xpath)))
            driver.find_element_by_xpath(sort_low_to_high_xpath).click()
            sleep(1.5)

            first_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/' \
                                 'div[3]/div[1]/span'
            first_result_text = float(driver.find_element_by_xpath(first_result_xpath).text.replace(",", "."))

            last_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[16]/div/div[2]/' \
                                'div[3]/div[1]/span'
            last_result_text = float(driver.find_element_by_xpath(last_result_xpath).text.replace(",", "."))

            self.assertLess(first_result_text, last_result_text)

        sleep(2)
        driver.find_element_by_xpath('//*[@id="select2-js-products-orderby-container"]').click()

        # Price from high to low test
        with self.subTest():
            sort_high_to_low_xpath = '/html/body/span/span/span[2]/ul/li[5]'
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_high_to_low_xpath)))
            driver.find_element_by_xpath(sort_high_to_low_xpath).click()
            sleep(1.5)

            first_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/' \
                                 'div[3]/div[1]/span[2]'
            first_result_text = float(driver.find_element_by_xpath(first_result_xpath).text.replace(",", "."))

            last_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[16]/div/div[2]/' \
                                'div[3]/div[1]/span'
            last_result_text = float(driver.find_element_by_xpath(last_result_xpath).text.replace(",", "."))

            self.assertGreater(first_result_text, last_result_text)

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
