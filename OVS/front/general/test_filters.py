import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    def test_filters(self):
        driver = self.driver

        man_category = driver.find_element_by_xpath('/html/body/div[9]/div/div[2]/ul[1]/li[2]/a')
        hover = ActionChains(driver).move_to_element(man_category)
        hover.perform()

        jeans_category_xpath = '/html/body/div[9]/div/div[2]/ul[1]/li[2]/div/div/div[1]/div[1]/div/div[1]/a'
        jeans_category = driver.find_element_by_xpath(jeans_category_xpath)
        jeans_category.click()

        # Price Filter
        with self.subTest():
            min_price = driver.find_element_by_id('min-price')
            pass

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
