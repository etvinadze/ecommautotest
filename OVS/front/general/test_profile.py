import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from OVS.essential_scripts.login_user import login_user
from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        self.go_to_profile()

    def go_to_profile(self):
        user_email = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='email.txt'
        )

        user_password = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='password.txt'
        )

        driver = self.driver
        login_user(
            driver=driver,
            username=user_email,
            password=user_password
        )

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, '/html/body/div[10]/div/div[1]/div[1]/ul/li[1]/a'))
        )

        driver.find_element_by_xpath('/html/body/div[10]/div/div[1]/div[1]/ul/li[1]/a').click()

    def test_profile_data_visibility(self):
        driver = self.driver

        sleep(2)

        self.assertTrue(driver.find_element_by_xpath(
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[1]/div[2]/div[2]/label'
        ).is_displayed())

        self.assertTrue(driver.find_element_by_id('FirstName').is_displayed())

        id_list = ['FirstName', 'LastName', 'Email', 'Company', 'Phone']
        xpath_list = [
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[1]/div[2]/div[3]',
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[1]/div[2]/div[4]',
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[1]/div[2]/div[5]',
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[2]/div[1]',
            '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/form/div[3]/div[1]',
        ]

        for html_id in id_list:
            with self.subTest(html_id=html_id):
                self.assertTrue(driver.find_element_by_id(html_id).is_displayed())

        for xpath in xpath_list:
            with self.subTest(xpath=xpath):
                self.assertTrue(driver.find_element_by_xpath(xpath).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
