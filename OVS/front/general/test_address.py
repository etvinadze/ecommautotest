import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from OVS.essential_scripts.login_user import login_user
from OVS.essential_scripts.get_information_from_text_files import get_information_from_text_files
from selenium.webdriver.common.action_chains import ActionChains


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(10)
        driver.maximize_window()

    def go_to_profile_addresses(self):
        user_email = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='email.txt'
        )

        user_password = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='password.txt'
        )

        driver = self.driver
        login_user(
            driver=driver,
            username=user_email,
            password=user_password
        )

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, '/html/body/div[10]/div[1]/div[1]/div[1]/ul/li[1]'))
        )

        sleep(0.5)
        profile_button = driver.find_element_by_xpath('/html/body/div[10]/div[1]/div[1]/div[1]/ul/li[1]')
        profile_button.click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[10]/div[1]/div[7]/div[2]/div/div[2]/ul/li[2]/a'
        )))

        sleep(1)
        addresses_button = driver.find_element_by_xpath('/html/body/div[10]/div[1]/div[7]/div[2]/div/div[2]/ul/li[2]/a')
        addresses_button.click()

    def test_address(self):
        driver = self.driver
        actions = ActionChains(driver)

        self.go_to_profile_addresses()

        with self.subTest():
            driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[2]/input'
            ).click()

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
                By.XPATH, '/html/body/div[10]/div[1]/div[7]/div[1]/form/div/div[2]/div[2]/input'))
            )
            sleep(1)

            driver.find_element_by_id('Address_FirstName').send_keys('gurbangul')

            driver.find_element_by_id('Address_LastName').send_keys('berdimuhamedov')

            driver.find_element_by_id('Address_Email').send_keys('fkjdashfkaj@gmail.com')

            driver.find_element_by_xpath('//*[@id="Address_StateProvinceId"]/option[2]').click()

            driver.find_element_by_id('Address_Address1').send_keys('ბიჩნიგაური')

            driver.find_element_by_id('Address_PhoneNumber').send_keys('595595959')

            # Save Address
            driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/form/div/div[2]/div[2]/input'
            ).click()

            self.assertEqual('GURBANGUL BERDIMUHAMEDOV', driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]/div[1]/strong'
            ).text)

            self.assertEqual('gurbangul berdimuhamedov', driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[1]'
            ).text)

            self.assertEqual('ელ. ფოსტა: fkjdashfkaj@gmail.com', driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[2]'
            ).text)

            self.assertEqual('ტელეფონი: 595595959', driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[3]'
            ).text)

            self.assertEqual('ბიჩნიგაური', driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[4]'
            ).text)

            self.assertEqual('თბილისი', driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[6]'
            ).text)

            self.assertEqual('Georgia', driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[7]'
            ).text)

        with self.subTest():
            sleep(1)

            driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/div[2]/input[1]'
            ).click()

            sleep(2)

            first_name = driver.find_element_by_id('Address_FirstName')
            first_name.clear()
            first_name.send_keys("gurbangull")

            driver.find_element_by_xpath(
                '/html/body/div[10]/div[1]/div[7]/div[1]/form/div/div[2]/div[2]/input'
            ).click()

            sleep(3)

            self.assertEqual("GURBANGULL BERDIMUHAMEDOV", driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/div[1]/strong'
            ).text)

            self.assertEqual('gurbangull berdimuhamedov', driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/ul/li[1]'
            ).text)

        with self.subTest():
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/div[2]/input[2]'
            )))

            actions.move_to_element(driver.find_element_by_xpath(
                '/html/body/div[11]/ul'
            )).perform()

            driver.find_element_by_xpath('//*[@id="facebook"]').click()

            driver.find_element_by_xpath(
                '/html/body/div[10]/div/div[7]/div[1]/div/div[2]/div[1]/div[2]/div[2]/input[2]'
            ).click()

            driver.switch_to.alert.accept()

            sleep(2)

            with self.assertRaises(NoSuchElementException):
                driver.find_element_by_xpath('/html/body/div[10]/div[1]/div[7]/div[1]/div/div[2]/div[1]/div[2]')

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
