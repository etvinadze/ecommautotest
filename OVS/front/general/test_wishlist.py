import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def add_item_to_wishlist(self):
        driver = self.driver
        driver.get("https://ovs.wandio.dev")

        # Search Product
        driver.find_element_by_id('small-searchterms').send_keys("Stretch Slim-Fit Chino Jeans")

        search_key_xpath = '//*[@id="small-search-box-form"]/input[2]'

        # Press Search Key
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, search_key_xpath)))
        driver.find_element_by_xpath(search_key_xpath).click()

        WebDriverWait(driver, 15).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[9]/div/div[7]/div/div[2]/div[3]/div/div/div/div/div[2]/div[3]/div[2]/input'
        )))

        sleep(1)

        # Wishlist Product
        driver.find_element_by_xpath(
            '/html/body/div[12]/div[2]/div[3]/form/div/div/div[2]/div[4]/input[2]'
        ).click()

        WebDriverWait(driver, 15).until(EC.element_to_be_clickable((
            '/html/body/div[14]/div[2]/div[3]/div/input'
        )))

        # Go to wishlist
        driver.find_element_by_xpath(
            '/html/body/div[13]/div[2]/div[3]/div/input'
        ).click()

    def test_delete_item_from_wishlist(self):
        driver = self.driver

        self.add_item_to_wishlist()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[9]/div[1]/div[7]/div/div/div[2]/div[1]/form/div[1]/table'
        )))

        delete_item_from_wishlist = driver.find_element_by_xpath(
            '/html/body/div[9]/div[1]/div[7]/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[2]/input'
        )
        delete_item_from_wishlist.click()

        sleep(1)
        wishlist_empty_message = driver.find_element_by_xpath('/html/body/div[9]/div[1]/div[7]/div/div/div[2]/div')
        self.assertEquals(wishlist_empty_message.text, "სურვილების სია ცარიელია!")

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
