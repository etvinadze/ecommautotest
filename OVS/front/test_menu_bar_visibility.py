import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_menu_bar_visibility(self):
        driver = self.driver

        driver.get("https://ovs.wandio.dev")
        menu_bar_xpath = '/html/body/div[9]/div[1]/div[2]/ul[1]'
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, menu_bar_xpath)))
        self.assertTrue(driver.find_element_by_xpath(menu_bar_xpath).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
