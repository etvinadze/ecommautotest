import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://ovs.wandio.dev")

    # ენის შესაცვლელს ვტესტავ "კონტაქტი"-ს ღილაკით. თუ ეგ ღილაკი შეიცვალა, ზნაჩიტ ენის გადამრთველიც მუშაობს
    def test_language_changer(self):
        driver = self.driver
        language_changer_button = driver.find_element_by_xpath('//*[@id="customerlanguage"]/option[2]')
        language_changer_button.click()
        register_xpath = '/html/body/div[9]/div/div[1]/div[1]/ul/li[1]/a'
        register_button = driver.find_element_by_xpath(register_xpath)
        WebDriverWait(driver, 40).until(EC.presence_of_element_located((
            By.XPATH, register_xpath
        )))
        self.assertEqual(register_button.text, "Register")


if __name__ == '__main__':
    unittest.main()
