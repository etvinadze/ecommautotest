from linecache import getline


def get_information_from_text_files(base_directory, directory, location):
    data = str(getline(f'{base_directory}\\{directory}\\{location}', 1))
    return data
