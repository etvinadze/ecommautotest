import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def add_item_to_wishlist(self):
        driver = self.driver
        driver.get("https://shop.wandio.dev")

        product_name = 'Apple MacBook Pro 13-inch'
        # Search Product
        driver.find_element_by_id('small-searchterms').send_keys(product_name)

        search_key_xpath = '/html/body/div[6]/div[1]/div[2]/div[2]/form/input[2]'

        # Press Search Key
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, search_key_xpath)))
        driver.find_element_by_xpath(search_key_xpath).click()

        WebDriverWait(driver, 15).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[6]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div[2]/h2'
        )))

        sleep(1)

        # Wishlist Product
        driver.find_element_by_xpath(
            '/html/body/div[6]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div[2]/div[3]/div[2]/input[3]'
        ).click()

        # WebDriverWait(driver, 15).until(EC.element_to_be_clickable((
        #     '/html/body/div[5]/div/p/a'
        # )))

        # Go to wishlist
        driver.find_element_by_xpath(
            '/html/body/div[5]/div/p/a'
        ).click()

    def test_item_show_update_and_delete_in_wishlist(self):
        driver = self.driver

        self.add_item_to_wishlist()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[6]/div[3]/div/div/div/div[2]'
        )))

        product_code = 'AP_MBP_13'

        # Test Product Code Visibility
        with self.subTest():
            self.assertEqual(
                product_code,
                driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[3]'
                ).text
            )

        # Test Product Picture Visibility
        with self.subTest():
            self.assertTrue(
                driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[4]/a/img'
                ).is_displayed()
            )

        # Test Product Name Visibility
        with self.subTest():
            self.assertEqual(
                'Apple MacBook Pro 13-inch',
                driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[5]'
                ).text
            )

        # Test Product Price Visibility
        with self.subTest():
            self.assertEqual(
                '1.800,00 GEL',
                driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[6]'
                ).text
            )

        # Test Update Button and Total price
        with self.subTest():
            product_number_xpath = '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/' \
                                   'td[7]/input'
            driver.find_element_by_xpath(product_number_xpath).clear()
            driver.find_element_by_xpath(product_number_xpath).send_keys(2)

            # Update Wishlist
            wishlist_update_xpath = '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[2]/input[1]'
            driver.find_element_by_xpath(wishlist_update_xpath).click()

            sleep(0.5)

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
                By.XPATH, wishlist_update_xpath
            )))

            self.assertEqual(
                '3.600,00 GEL',
                driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[8]'
                ).text
            )

        # Test Product Delete Box
        with self.subTest():
            # Click Delete Product Box
            driver.find_element_by_xpath(
                '/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[1]/input'
            ).click()

            driver.find_element_by_xpath(wishlist_update_xpath).click()

            # თუ კალათაში დამატება ან წაშლა არ შემიძლია, ესეიგი ვიშლისთი ცარიელია
            with self.assertRaises(NoSuchElementException):
                driver.find_element_by_xpath('/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[2]')

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
