import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get('https://shop.wandio.dev/electronics')

    def test_grid_list_view(self):
        driver = self.driver

        # Grid View
        with self.subTest():
            driver.find_element_by_id('js-grid-mode').click()

            sleep(2)

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[2]'))
            )

            self.assertEqual(True, driver.find_element_by_xpath(
                '//*[@id="filter-product-container"]/div/div/div[2]/div[4]'
            ).is_displayed())

        # List View
        with self.subTest():
            driver.find_element_by_id('js-list-mode').click()

            sleep(1)

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[2]'))
            )

            self.assertEqual(True, driver.find_element_by_xpath(
                '//*[@id="filter-product-container"]/div/div/div[2]/div[4]'
            ).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
