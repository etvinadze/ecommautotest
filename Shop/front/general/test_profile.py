import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Shop.essential_scripts.login_user import login_user
from Shop.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        self.go_to_profile()

    def go_to_profile(self):
        user_email = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='email.txt'
        )

        user_password = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='password.txt'
        )

        driver = self.driver
        login_user(
            driver=driver,
            username=user_email,
            password=user_password
        )

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, '/html/body/div[6]/div[2]/div[1]/div[2]/div[1]/ul/li[1]'))
        )

        driver.find_element_by_xpath('/html/body/div[6]/div[2]/div[1]/div[2]/div[1]/ul/li[1]').click()

    def test_profile_data_visibility(self):
        driver = self.driver

        sleep(2)

        self.assertTrue(driver.find_element_by_xpath(
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]'
        ).is_displayed())

        id_list = ['FirstName', 'LastName', 'Email', 'Company', 'Phone']
        xpath_list = [
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]/div[1]/div/span[1]',  # მდ. სქესი
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]/div[1]/div/span[2]',  # მამრ. სქესი
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]/div[4]/div/select[1]',  # დაბადების დღე
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]/div[4]/div/select[2]',  # დაბადების თვე
            '/html/body/div[6]/div[4]/div/div[2]/div/div[2]/form/div[1]/div[2]/div[4]/div/select[3]',  # დაბადების წელი
            '//*[@id="save-info-button"]'  # პროფილის შენახვა
        ]

        for html_id in id_list:
            with self.subTest(html_id=html_id):
                self.assertTrue(driver.find_element_by_id(html_id).is_displayed())

        for xpath in xpath_list:
            with self.subTest(xpath=xpath):
                self.assertTrue(driver.find_element_by_xpath(xpath).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
