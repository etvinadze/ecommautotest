import unittest
from re import findall
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get('https://shop.wandio.dev/electronics')

    def test_sorts(self):
        driver = self.driver

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.ID, 'select2-js-products-orderby-container'))
        )

        driver.find_element_by_id('select2-js-products-orderby-container').click()

        # A-Z Sort
        with self.subTest():
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((
                By.XPATH, '/html/body/span/span/span[2]/ul/li[2]'
            )))

            driver.find_element_by_xpath('/html/body/span/span/span[2]/ul/li[2]').click()

            sleep(3)

            first_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[3]/div/div[1]/div/div[2]/h2'
            first_result_text = driver.find_element_by_xpath(first_result_xpath).text
            first_result_text.split()
            first_result_word = first_result_text[0]

            driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[5]').click()
            sleep(3)

            last_result_xpath = '//*[@id="filter-product-container"]/div/div/div[2]/div[3]/div/div[5]/div/div[2]/h2'
            last_result_text = driver.find_element_by_xpath(last_result_xpath).text
            last_result_text.split()
            last_result_word = last_result_text[0]

            self.assertEqual(first_result_word, "A")
            self.assertEqual(last_result_word, "U")

        # პირველ გვერდზე დაბრუნება
        driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[2]').click()
        sleep(3)

        # Z-A Sort
        with self.subTest():
            driver.find_element_by_id('select2-js-products-orderby-container').click()

            sort_z_a_xpath = '/html/body/span/span/span[2]/ul/li[3]'

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_z_a_xpath)))
            driver.find_element_by_xpath(sort_z_a_xpath).click()

            sleep(3)

            first_result_text = driver.find_element_by_xpath(first_result_xpath).text
            first_result_text.split()
            first_result_word = first_result_text[0]

            # მეხუთე გვერდზე გადასვლა
            driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[5]').click()
            sleep(3)

            last_result_text = driver.find_element_by_xpath(last_result_xpath).text
            last_result_text.split()
            last_result_word = last_result_text[0]

            self.assertEqual(first_result_word, "ს")
            self.assertEqual(last_result_word, "A")

        # პირველ გვერდზე გადასვლა
        driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[2]').click()
        sleep(2)

        # ფასი ზრდადობით
        with self.subTest():
            driver.find_element_by_id('select2-js-products-orderby-container').click()

            sort_low_to_high_xpath = '/html/body/span/span/span[2]/ul/li[4]'
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_low_to_high_xpath)))

            driver.find_element_by_xpath(sort_low_to_high_xpath).click()

            sleep(3)

            first_result_xpath = '/html/body/div[6]/div[3]/div/div[2]/div/div/div/div/div[2]/div[3]/div/div[1]/div/' \
                                 'div[2]/div[3]/div[1]/span'
            first_result_text = findall(
                "\d+\.\d+",
                driver.find_element_by_xpath(first_result_xpath).text.replace(",", ".")
            )[0]

            # მეხუთე გვერდზე გადასვლა
            driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[5]').click()
            sleep(3)

            last_result_xpath = '/html/body/div[6]/div[3]/div/div[2]/div/div/div/div/div[2]/div[3]/div/div[5]/div/' \
                                'div[2]/div[3]/div[1]/span'

            last_result_text = findall(
                "\d+\.\d+",
                driver.find_element_by_xpath(last_result_xpath).text.replace(",", ".")
            )[0]
            self.assertLess(first_result_text, last_result_text)

        # პირველ გვერდზე გადასვლა
        driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[2]').click()
        sleep(2)

        # ფასი კლებადობით
        with self.subTest():
            driver.find_element_by_id('select2-js-products-orderby-container').click()

            sort_high_to_low_xpath = '/html/body/span/span/span[2]/ul/li[5]'
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, sort_high_to_low_xpath)))
            driver.find_element_by_xpath(sort_high_to_low_xpath).click()

            sleep(3)

            first_result_xpath = '/html/body/div[6]/div[3]/div/div[2]/div/div/div/div/div[2]/div[3]/div/div[1]/div/' \
                                 'div[2]/div[3]/div[1]/span'
            first_result_text = findall(
                "\d+\.\d+",
                driver.find_element_by_xpath(first_result_xpath).text.replace(",", ".")
            )[0]

            # მეხუთე გვერდზე გადასვლა
            driver.find_element_by_xpath('//*[@id="filter-product-container"]/div/div/div[2]/div[4]/ul/li[6]').click()
            sleep(3)

            last_result_xpath = '/html/body/div[6]/div[3]/div/div[2]/div/div/div/div/div[2]/div[3]/div/div[5]/div/' \
                                'div[2]/div[3]/div[1]/span'
            last_result_text = findall(
                "\d+\.\d+",
                driver.find_element_by_xpath(last_result_xpath).text.replace(",", ".")
            )[0]

            self.assertGreater(first_result_text, last_result_text)

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
