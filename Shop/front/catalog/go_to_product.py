from time import sleep
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


def go_to_product(driver):
    driver = driver

    man_category = driver.find_element_by_xpath('/html/body/div[9]/div/div[2]/ul[1]/li[2]/a')
    hover = ActionChains(driver).move_to_element(man_category)
    hover.perform()

    jeans_category_xpath = '/html/body/div[9]/div/div[2]/ul[1]/li[2]/div/div/div[1]/div[1]/div/div[1]/a'
    jeans_category = driver.find_element_by_xpath(jeans_category_xpath)
    jeans_category.click()

    WebDriverWait(driver, 10).until(EC.presence_of_element_located((
        By.XPATH, '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
    )))

    sleep(1)

    first_product = driver.find_element_by_xpath(
        '//*[@id="filter-product-container"]/div/div/div[2]/div[4]/div/div[1]/div/div[2]/h2/a'
    )
    first_product.click()
