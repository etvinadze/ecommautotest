import unittest
from selenium import webdriver


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get("https://shop.wandio.dev/htc-one-m8-android-l-50-lollipop")

    def test_product_details_visibility(self):
        driver = self.driver

        product_picture_xpath = '//*[@id="product-details-form"]/div/div[1]/div[1]/div'
        product_full_name_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[1]'
        product_short_description_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[2]'
        product_rating_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[3]'
        product_rate_yourself_button_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[3]/div[2]'
        product_in_stock_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[4]'
        product_code_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[5]'
        product_attributes_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[6]'
        product_price_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[7]'
        product_add_to_cart_number_xpath = '/html/body/div[6]/div[3]/div[2]/div/div/div/form/div/div[1]/div[2]/div[8]/div/input[1]'
        product_add_to_cart_xpath = '/html/body/div[6]/div[3]/div[2]/div/div/div/form/div/div[1]/div[2]/div[8]/div/input[2]'
        product_add_to_wishlist_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[9]/div[1]'
        product_compare_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[9]/div[2]'
        product_send_to_friend_xpath = '//*[@id="product-details-form"]/div/div[1]/div[2]/div[9]/div[3]'
        product_full_description = '//*[@id="product-details-form"]/div/div[1]/div[3]'
        product_specifications_xpath = '//*[@id="product-details-form"]/div/div[2]/div[1]'
        product_tags_xpath = '//*[@id="product-details-form"]/div/div[2]/div[2]'

        product_details_list = [
            product_picture_xpath,
            product_full_name_xpath,
            product_short_description_xpath,
            product_rating_xpath,
            product_rate_yourself_button_xpath,
            product_in_stock_xpath,
            product_code_xpath,
            product_attributes_xpath,
            product_price_xpath,
            product_add_to_cart_number_xpath,
            product_add_to_cart_xpath,
            product_add_to_wishlist_xpath,
            product_compare_xpath,
            product_send_to_friend_xpath,
            product_full_description,
            product_specifications_xpath,
            product_tags_xpath
        ]

        for product_detail in product_details_list:
            with self.subTest(product_detail=product_detail):
                self.assertTrue(driver.find_element_by_xpath(product_detail).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
