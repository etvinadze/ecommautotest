import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from Shop.essential_scripts.login_user import login_user
from Shop.essential_scripts.get_information_from_text_files import get_information_from_text_files


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_login_on_wrong_credentials(self):
        driver = self.driver

        profile_xpath = '/html/body/div[6]/div[2]/div[1]/div[2]/div[1]/ul/li[1]/a'

        login_user(
            driver=driver,
            username="jafjkhfjkads@wandio.com",
            password="fdaskj312321"
        )

        with self.assertRaises(NoSuchElementException):
            driver.find_element_by_xpath(profile_xpath)

    def test_login_on_right_credentials(self):
        driver = self.driver

        profile_xpath = '/html/body/div[6]/div[2]/div[1]/div[2]/div[1]/ul/li[1]/a'

        email_location = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='email.txt'
        )

        password_location = get_information_from_text_files(
            base_directory='C:\\Users\\Tombstone\\Desktop\\ecomm_test_credentials',
            directory='right_credentials',
            location='password.txt'
        )

        login_user(
            driver=driver,
            username=email_location,
            password=password_location
        )

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, profile_xpath)))
        profile = driver.find_element_by_xpath(profile_xpath)

        self.assertTrue(profile.is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
