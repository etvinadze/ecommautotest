import unittest
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Shop.admin.users.go_to_admin_users import go_to_admin_users


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_filters(self):
        driver = self.driver
        go_to_admin_users(driver=driver)

        # Email Filter
        with self.subTest():
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'SearchEmail')))
            email_filter = driver.find_element_by_id('SearchEmail')
            email_filter.send_keys('erekle.tvinadze@wandio.com')
            driver.find_element_by_id('search-customers').click()

            sleep(1)
            result_email = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[2]').text

            self.assertEqual('erekle.tvinadze@wandio.com', result_email)

        # First and Last name Filter
        with self.subTest():
            email_filter.clear()
            driver.find_element_by_id('search-customers').click()

            first_name = driver.find_element_by_id('SearchFirstName')
            last_name = driver.find_element_by_id('SearchLastName')
            first_name.send_keys('erekle')
            last_name.send_keys('tvinadze')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_name = driver.find_element_by_xpath('//*[@id="customers-grid"]/tbody/tr/td[3]').text
            self.assertEqual('erekle tvinadze', result_name)

        # Company Filter
        with self.subTest():
            first_name.clear()
            last_name.clear()
            driver.find_element_by_id('search-customers').click()

            company = driver.find_element_by_id('SearchCompany')
            company.send_keys('wandio')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_company = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div[2]/table/tbody/tr/td[5]').text
            self.assertEqual('wandio', result_company)

        # Phone Filter
        with self.subTest():
            company.clear()
            driver.find_element_by_id('search-customers').click()

            phone = driver.find_element_by_id('SearchPhone')
            phone.send_keys('551500291')

            driver.find_element_by_id('search-customers').click()
            sleep(1)

            result_phone = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div[2]/table/tbody/tr/td[6]').text
            self.assertEqual('551500291', result_phone)

        phone.clear()
        driver.find_element_by_id('search-customers').click()

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
