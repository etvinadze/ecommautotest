from Shop.admin.go_to_admin import go_to_admin


def go_to_admin_users(driver):
    go_to_admin(driver=driver)

    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[4]').click()

    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[4]/ul/li[1]/a').click()
