import unittest
from selenium import webdriver
from Shop.admin.manage_content.go_to_admin_manage_content import go_to_admin_manage_content


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_sms_templates(self):
        driver = self.driver
        go_to_admin_manage_content(driver=driver)

        sms_templates = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[6]/ul/li[9]/a')
        sms_templates.click()


if __name__ == '__main__':
    unittest.main()
