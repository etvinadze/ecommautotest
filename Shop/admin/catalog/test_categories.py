import unittest
from Shop.admin.catalog.go_to_catalog import go_to_catalog
from selenium import webdriver


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_categories(self):
        driver = self.driver
        go_to_catalog(driver=driver)

        category = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[2]/ul/li[2]/a')
        category.click()

        # Export/Import/Add button and visibility Test
        xpath_list = [
            '/html/body/div[3]/div[3]/div/div[1]/div/a',
            '/html/body/div[3]/div[3]/div/div[1]/div/div/button[1]',
            '/html/body/div[3]/div[3]/div/div[1]/div/button'
        ]
        for xpath_element in xpath_list:
            with self.subTest(element=xpath_element):
                self.assertTrue(driver.find_element_by_xpath(xpath_element).is_displayed())

        # კატეგორიების რაოდენობა არის თუ არა სწორად გამოსახული
        with self.subTest():
            for i in range(1, 16):
                self.assertTrue(driver.find_element_by_xpath(
                    f'/html/body/div[3]/div[3]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div[2]/table/'
                    f'tbody/tr[{i}]'
                ).is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
