import unittest
from Shop.admin.catalog.go_to_catalog import go_to_catalog
from time import sleep
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_products(self):
        driver = self.driver
        go_to_catalog(driver=driver)

        save_and_edit_xpath = '//*[@id="product-form"]/div[1]/div/button[3]'
        product_name_xpath = '//*[@id="Locales_0__Name"]'
        product_front_name_xpath = '/html/body/div[6]/div[4]/div[2]/div/div/div/form/div/div[1]/div[2]/div[1]'
        product_front_short_description_xpath = '/html/body/div[6]/div[4]/div[2]/div/div/div/form/div/div[1]/div[2]/div[2]'
        dismiss_button_xpath = '/html/body/div[3]/div[3]/div[1]/button'

        actions = ActionChains(driver)
        add_combination_url = 'https://shop.wandio.dev/Admin/Product/ProductAttributeCombinationCreatePopup?productId=60883&btnId=btnRefreshCombinations&formId=product-form'

        first_tab = driver.window_handles[0]
        driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[2]/ul/li[1]/a').click()

        search_button = driver.find_element_by_id('search-products')

        # Go to Product Details
        product_test_name = 'test product'
        driver.find_element_by_id('SearchProductName').send_keys(product_test_name)
        search_button.click()

        sleep(1)

        edit_product = driver.find_element_by_xpath(
            '/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/table/'
            'tbody/tr[1]/td[9]/a'
        )
        edit_product.click()

        WebDriverWait(driver, 15).until(EC.element_to_be_clickable((
            By.ID, 'product-delete'))
        )

        driver.execute_script("window.open('https://shop.wandio.dev/%E1%83%A1%E1%83%90%E1%83%A2%E1%83%94%E1%83%A1%E1%83%A2%E1%83%9D-%E1%83%A1%E1%83%90%E1%83%AE%E1%83%94%E1%83%9A%E1%83%98');")

        second_tab = driver.window_handles[1]

        driver.switch_to.window(first_tab)

        # Change Product Name
        with self.subTest():
            driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[1]/div/'
                                         'div[2]/div/div[1]/ul/li[2]/a').click()

            product_test_name = 'სატესტო სახელი'
            driver.find_element_by_xpath(product_name_xpath).clear()
            driver.find_element_by_xpath(product_name_xpath).send_keys(product_test_name)

            sleep(1)

            # შენახვა და რედაქტირება
            driver.find_element_by_xpath(save_and_edit_xpath).click()

            sleep(1)

            driver.switch_to.window(second_tab)
            driver.refresh()

            product_front_name = driver.find_element_by_xpath(product_front_name_xpath).text
            self.assertEqual("სატესტო სახელი", product_front_name)

        driver.switch_to.window(first_tab)

        # პროდუქტის "მთავარი"-ში გადასვლა
        driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/form/div[3]/div/nop-panels/nop-panel[1]/div/'
                                     'div[2]/div/div[1]/ul/li[1]/a').click()
        sleep(0.5)

        driver.find_element_by_xpath(dismiss_button_xpath).click()

        # Change Product short description
        with self.subTest():
            product_short_description = 'შაურმა ბევრი ხორცით და ნაკლები წიწაკით'
            driver.find_element_by_id('ShortDescription').clear()
            driver.find_element_by_id('ShortDescription').send_keys(product_short_description)

            sleep(1)

            driver.find_element_by_xpath(save_and_edit_xpath).click()

            sleep(0.5)

            driver.switch_to.window(second_tab)
            driver.refresh()

            self.assertEqual(
                product_short_description,
                driver.find_element_by_xpath(product_front_short_description_xpath).text
            )

        driver.switch_to.window(first_tab)
        driver.find_element_by_xpath(dismiss_button_xpath).click()

        # Change product full description
        with self.subTest():
            self.skipTest("სრული აღწერის ტესტი. ვერ ვარედაქტირებ ტექსტს და ვერ ვხვდები რატომ")
            driver.find_element_by_id("tinymce").clear()
            driver.find_element_by_id("tinymce").send_keys('ქურდული ტანსაცმელი ქურდული გაგებით')

            sleep(0.5)

            driver.find_element_by_xpath(save_and_edit_xpath).click()
            driver.switch_to.window(second_tab)
            driver.refresh()

            driver.find_element_by_id('quickTab-description')

            self.assertEqual(
                product_short_description,
                driver.find_element_by_id('quickTab-description').text
            )

        driver.switch_to.window(second_tab)

        # Add Combination Test
        with self.subTest():
            driver.execute_script(f"window.open('{add_combination_url}');")

            third_tab = driver.window_handles[2]
            driver.switch_to.window(third_tab)

            # Color
            driver.find_element_by_xpath('/html/body/div/div/form/div[2]/div[2]/div/div/div/div[1]/div[2]/div/div[1]/label').click()

            # Size
            driver.find_element_by_xpath('/html/body/div/div/form/div[2]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/label').click()

            # Stock size
            driver.find_element_by_id('StockQuantity').send_keys('1597')

            # Product unique identifier
            driver.find_element_by_id('Sku').send_keys('6665')

            # Product Old Price
            driver.find_element_by_id('OverriddenPrice').send_keys('100.0000')

            # Product New Price
            driver.find_element_by_id('OldOverriddenPrice').send_keys('200.0000')

            # Product Picture
            driver.find_element_by_xpath('//*[@id="id_image_268"]').click()

            # Save New Combination
            driver.find_element_by_xpath('/html/body/div/div/form/div[1]/div/button').click()

            driver.switch_to.window(first_tab)
            driver.refresh()

            # Go to Combination list
            driver.find_element_by_xpath('//*[@id="productattribute-edit"]/ul/li[2]/a').click()

            sleep(1)

            self.assertTrue(driver.find_element_by_xpath(
                '/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr'
            ).is_displayed())

        # Remove Combination
        with self.subTest():
            # Remove Combination
            driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[12]/a').click()
            driver.switch_to.alert.accept()
            sleep(3)

            actions.move_to_element(driver.find_element_by_xpath(
                '//*[@id="product-specification-attributes"]/div[1]'
            )).perform()

            # Refresh Combination Table
            driver.find_element_by_xpath(
                '//*[@id="attributecombinations-grid_wrapper"]/div[2]/div[4]/div/div/button'
            ).click()

            sleep(1)

            with self.assertRaises(NoSuchElementException):
                driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[2]')

        # Go Back to Product List
        driver.find_element_by_xpath('//*[@id="product-form"]/div[1]/h1/small').click()

        # Product Name Search
        with self.subTest():
            product_test_name = 'test product'
            driver.find_element_by_id('SearchProductName').send_keys(product_test_name)

            driver.find_element_by_id('search-products').click()

            sleep(2)

            result_product_name = driver.find_element_by_xpath('//*[@id="products-grid"]/tbody/tr/td[3]').text
            self.assertEqual(product_test_name, result_product_name)

        sleep(0.5)

        driver.find_element_by_id('SearchProductName').clear()
        driver.find_element_by_id('search-products').click()

        sleep(3)

        # Product Type Filter
        with self.subTest():
            driver.find_element_by_xpath('//*[@id="SearchProductTypeId"]/option[2]').click()
            driver.find_element_by_id('search-products').click()

            sleep(3)

            for i in range(1, 16):
                result_product_type = driver.find_element_by_xpath(
                    f'/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]'
                    f'/table/tbody/tr[{i}]/td[7]').text

                self.assertEqual('ჩვეულებრივი', result_product_type)

        driver.find_element_by_xpath('//*[@id="SearchProductTypeId"]/option[1]').click()
        driver.find_element_by_id('search-products').click()

        sleep(3)

        # Test Published Filter
        with self.subTest():
            driver.find_element_by_xpath('/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[1]/div/div[2]/div[1]'
                                         '/div[2]/div[4]/div[2]/select/option[2]').click()
            driver.find_element_by_id('search-products').click()

            sleep(1)

            for i in range(1, 16):
                self.assertTrue(driver.find_element_by_xpath(
                    f'/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]'
                    f'/table/tbody/tr[{i}]/td[8]'
                ).is_displayed())

        driver.find_element_by_xpath(
            '/html/body/div[3]/div[3]/div/form[1]/div[2]/div/div/div[1]/div/div[2]/div[1]/div[2]/div[4]/div[2]/select/'
            'option[2]'
        ).click()

        driver.find_element_by_id('search-products').click()

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
