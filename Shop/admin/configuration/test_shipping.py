import unittest
from selenium import webdriver
from Shop.admin.configuration.go_to_admin_configuration import go_to_admin_configuration


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_shipping(self):
        driver = self.driver
        go_to_admin_configuration(driver=driver)

        shipping = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[7]/ul/li[11]/a')
        shipping.click()

        shipping_providers = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[7]/ul/li[11]/ul/li[1]/a')
        shipping_providers.click()


if __name__ == '__main__':
    unittest.main()
