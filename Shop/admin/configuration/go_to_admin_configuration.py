from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Shop.admin.go_to_admin import go_to_admin


def go_to_admin_configuration(driver):
    go_to_admin(driver=driver)

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
        By.XPATH, '/html/body/div[3]/div[2]/div/ul/li[6]/a'))
    )
    configuration = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[7]/a')
    configuration.click()
