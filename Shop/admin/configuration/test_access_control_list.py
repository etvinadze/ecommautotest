import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Shop.admin.configuration.go_to_admin_configuration import go_to_admin_configuration


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_access_control_list(self):
        driver = self.driver
        go_to_admin_configuration(driver=driver)

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((
            By.XPATH, '/html/body/div[3]/div[2]/div/ul/li[7]/ul/li[12]/a'))
        )
        access_control_list = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[7]/ul/li[12]/a')
        access_control_list.click()

        access_control_list_front = driver.find_element_by_xpath(
            '/html/body/div[3]/div[3]/div/form/div[2]/div/div/div/div'
        )
        access_control_list_front.click()

        self.assertTrue(access_control_list_front.is_displayed())

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
