import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Shop.admin.sales.go_to_sales import go_to_sales


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome("C:/Users/Tombstone/Desktop/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()

    def test_orders(self):
        driver = self.driver

        go_to_sales(driver=driver)

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, '/html/body/div[3]/div[2]/div/ul/li[2]/a'))
        )

        orders = driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/ul/li[3]/ul/li[1]/a')
        orders.click()


if __name__ == '__main__':
    unittest.main()
