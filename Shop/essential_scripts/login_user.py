from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep


def login_user(driver, username, password):

    driver.get("https://shop.wandio.dev")
    login_button_xpath = '/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a'
    WebDriverWait(driver, 40).until(EC.visibility_of_element_located((By.XPATH, login_button_xpath)))
    login_button = driver.find_element_by_xpath(login_button_xpath)

    login_button.click()

    email_bar = driver.find_element_by_id('Email')
    sleep(0.5)
    email_bar.send_keys(username)

    sleep(0.5)

    password_bar = driver.find_element_by_id('Password')
    sleep(0.5)
    password_bar.send_keys(password)
