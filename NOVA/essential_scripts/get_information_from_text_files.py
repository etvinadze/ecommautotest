from linecache import getline


def get_information_from_text_files(directory):
    data = str(getline(f'{directory}', 1)).split('\n')[0]
    return data
