from NOVA.default_config import nova_url
from time import sleep


def login_user(driver, username, password):
    driver.get(nova_url)

    login_button_xpath = "/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/div[2]/form/div[3]/input"

    driver.find_element_by_link_text(u"Log in").click()

    sleep(1)

    driver.find_element_by_id("Email").clear()
    driver.find_element_by_id("Email").send_keys(username)
    driver.find_element_by_id("Password").clear()
    driver.find_element_by_id("Password").send_keys(password)
    driver.find_element_by_xpath(login_button_xpath).click()
