from NOVA.essential_scripts.login_user import login_user
from NOVA.default_config import right_username, right_password
from time import sleep


def go_to_admin(driver):
    login_user(
        driver=driver,
        username=right_username,
        password=right_password
    )

    sleep(1)
    administration = driver.find_element_by_xpath('/html/body/div[6]/div[1]/a')
    administration.click()
