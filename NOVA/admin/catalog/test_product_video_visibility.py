import unittest
from NOVA.default_config import webdriver_location, nova_url, right_username, right_password
from NOVA.essential_scripts.login_user import login_user
from time import sleep
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome(webdriver_location)
        driver = self.driver
        driver.implicitly_wait(10)
        driver.maximize_window()

    def test_product_video_visibility(self):
        driver = self.driver

        # კონკრეტული პროდუქტის URL
        product_main_url = f"{nova_url}/Admin/Product/Edit/55"

        # ვიდეოს დაედიტების ღილაკის Xpath
        video_edit_url_xpath = '/html/body/div[3]/div[3]/div[2]/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]' \
                               '/div[1]/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[1]/input'

        video_edit_button_xpath = '/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/' \
                                  'div[1]/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[3]/a[1]'

        video_save_button_xpath = '/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/' \
                                  'div[1]/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[3]/a[2]'

        video_delete_button_xpath = '/html/body/div[3]/div[3]/div[2]/form/div[3]/div/nop-panels/nop-panel[6]/div/' \
                                    'div[2]/div[1]/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[4]/a'

        video_url_text_location = '/html/body/div[3]/div[3]/div/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/' \
                                  'div[1]/div[1]/div/div[1]/div/div/div[2]/table/tbody/tr/td[1]'

        save_and_edit_button_xpath = '//button[3]'

        '''
        !!! ტესტის დაწყება !!!
        '''

        login_user(
            driver,
            right_username,
            right_password
        )

        driver.get(product_main_url)

        admin_window = driver.window_handles[0]

        # ვიდეოს URL-ის ჩაწერა
        driver.find_element_by_id('AddVideoModel_VideoUrl').send_keys('https://www.youtube.com/watch?v=AKf7QKTmJSg')

        with self.subTest():
            # ვიდეოს შენახვა
            driver.find_element_by_id('addProductVideo').click()

            # შენახვა და რედაქტირება
            driver.find_element_by_xpath(save_and_edit_button_xpath).click()

            # იმის შემოწმება პროდუქტის დეტალებში დაემატა თუ არა ვიდეო
            self.assertTrue(driver.find_element_by_xpath(
                '/html/body/div[3]/div[3]/div[2]/form/div[3]/div/nop-panels/nop-panel[6]/div/div[2]/div[1]/div[1]/div/'
                'div[1]/div/div/div[2]/table/tbody/tr'
            ).is_displayed())

        # პროდუქტის ფრონტზე გადასვლა
        driver.find_element_by_xpath("//button[@class='btn bg-purple']").click()

        # მეორე ფანჯარაზე ფოკუსის გადატანა
        front_window = driver.window_handles[1]
        driver.switch_to.window(front_window)
        driver.maximize_window()

        # URL-ის შემოწმება
        with self.subTest():
            self.assertEqual(
                driver.current_url,
                "https://nova.wandio.dev/notebooks/satesto-produqti-video-2"
            )

        # ვიდეოს ტესტი ფრონტზე
        with self.subTest():
            driver.find_element_by_xpath(
                '//*[@id="product-details-form"]/div[2]/div[1]/div[1]/div[2]/div/img[2]'
            ).click()

            # ვიდეოს დაჭერის შემდეგ ჩანს თუ არა ვიდეო
            self.assertTrue(
                driver.find_element_by_xpath(
                    '//*[@id="product-details-form"]/div[2]/div[1]/div[1]/div[1]/div'
                ).is_displayed()
            )

        # ადმინის თაბზე დაბრუნება
        driver.switch_to.window(admin_window)

        # ვიდეოს URL-ის და-Edit-ების ტესტი
        with self.subTest():
            # რედაქტირებაზე დაჭერა
            driver.find_element_by_xpath(video_edit_button_xpath).click()

            # სხვა URL-ის ჩაწერა
            driver.find_element_by_xpath(video_edit_url_xpath).clear()
            driver.find_element_by_xpath(video_edit_url_xpath).send_keys('https://www.youtube.com/watch?v=3WAOxKOmR90')

            # ვიდეო ედიტის შენახვა
            driver.find_element_by_xpath(video_save_button_xpath).click()

            sleep(1)

            # დაედიტების შემდეგ შეინახა თუ არა ახალი URL
            self.assertEqual(
                driver.find_element_by_xpath(video_url_text_location).text,
                "https://www.youtube.com/watch?v=3WAOxKOmR90"
            )

        # ვიდეოს წაშლის ტესტი
        with self.subTest():
            # წაშლაზე დაჭერა
            driver.find_element_by_xpath(video_delete_button_xpath).click()

            # Confirm
            driver.switch_to.alert.accept()

            sleep(2)

            # დადასტურება, რომ ელემენტი წაიშალა და ადმინში აღარ ჩანს
            self.assertNotEqual(
                driver.find_element_by_xpath(video_url_text_location).text,
                "https://www.youtube.com/watch?v=3WAOxKOmR90"
            )

        # ცვლილებების შენახვა
        driver.find_element_by_xpath(save_and_edit_button_xpath).click()

        # ფრონტზე გადასვლა ვიდეოს წაშლის დადასტურებისთვის
        driver.switch_to.window(front_window)
        driver.refresh()

        # ვიდეოს ფრონტზე გაქრობის დასტური
        with self.subTest():
            with self.assertRaises(NoSuchElementException):
                driver.find_element_by_xpath('/html/body/div[6]/div[4]/div[2]/div/div/div/form/div[2]/div[1]/div[1]/'
                                             'div[2]/div')


if __name__ == '__main__':
    unittest.main()
