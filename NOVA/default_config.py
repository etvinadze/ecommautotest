from NOVA.essential_scripts.get_information_from_text_files import get_information_from_text_files

webdriver_location = 'C:\\Users\\PC\\Desktop\\chromedriver.exe'

right_username = get_information_from_text_files(
    directory='C:\\Users\\PC\\Desktop\\credentials\\ecomm_test_credentials\\right_credentials\\email.txt'
)

right_password = get_information_from_text_files(
    directory='C:\\Users\\PC\\Desktop\\credentials\\ecomm_test_credentials\\right_credentials\\password.txt'
)

nova_url = 'https://nova.wandio.dev'
